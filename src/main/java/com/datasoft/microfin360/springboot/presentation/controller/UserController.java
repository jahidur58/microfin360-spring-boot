package com.datasoft.microfin360.springboot.presentation.controller;

import com.datasoft.microfin360.springboot.model.Role;
import com.datasoft.microfin360.springboot.model.User;
import com.datasoft.microfin360.springboot.service.RoleService;
import com.datasoft.microfin360.springboot.service.UserService;
import com.datasoft.microfin360.springboot.validation.UserValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jahid on 9/24/18.
 */

@Controller
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("users", userService.getAll());
        return "users/index";
    }

    @RequestMapping(value={"/users/save"}, method = RequestMethod.GET)
    public String add(Model model, @PathVariable(required = false, name = "id") Long id) {

            log.error("Role List {}", roleService.getAll().size());
            model.addAttribute("roles", roleService.getAll());
            model.addAttribute("user", new User());


        return "users/save";
    }

    @RequestMapping(value={"/users/edit/{id}"}, method = RequestMethod.GET)
    public String edit(Model model, @PathVariable(required = false, name = "id") Long id) {

        if (null != id)
            model.addAttribute("user", userService.get(id));

        return "users/edit";
    }

    @RequestMapping(value = "/users/save", method = RequestMethod.POST)
    public String insert(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "users/save";
        }
        if(user.getId() > 0) {
            userService.edit(user);
        }
        else {
            userService.create(user);
        }


        model.addAttribute("users", userService.getAll());
        return "users/index";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.POST)
    public String update(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {

            userService.edit(user);


        model.addAttribute("users", userService.getAll());
        return "users/index";
    }




    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable(required = true, name = "id") Long id) {
        userService.delete(id);
        model.addAttribute("users", userService.getAll());
        return "users/index";
    }
}
