package com.datasoft.microfin360.springboot.presentation.controller;

import com.datasoft.microfin360.springboot.model.Member;
import com.datasoft.microfin360.springboot.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jahid on 9/26/18.
 */

@Controller
public class MemberController {

    @Autowired
    private MemberService memberService;



    @RequestMapping(value = "/members", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("members", memberService.getAll());
        return "pages/home";
    }

    @RequestMapping(value={"/members/save", "/members/save/{id}"}, method = RequestMethod.GET)
    public String add(Model model, @PathVariable(required = false, name = "id") Long id) {
        if (null != id) {
            model.addAttribute("member", memberService.get(id));
        } else {
            model.addAttribute("member", new Member());
        }
        return "members/save";
    }

    @RequestMapping(value = "/members/save", method = RequestMethod.POST)
    public String insert(@ModelAttribute Member member, Model model) {
        if(member.getId() > 0) {
            memberService.edit(member);
        }
        else {
            memberService.create(member);
        }
        model.addAttribute("members", memberService.getAll());
        return "members/index";
    }

    @RequestMapping(value = "/members/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable(required = true, name = "id") Long id) {
        memberService.delete(id);

        model.addAttribute("members", memberService.getAll());
        return "members/index";
    }
}
