package com.datasoft.microfin360.springboot.presentation.controller;

import com.datasoft.microfin360.springboot.model.Role;
import com.datasoft.microfin360.springboot.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jahid on 9/26/18.
 */

@Controller
public class RoleController {

    @Autowired
    private RoleService roleService;



    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("roles", roleService.getAll());
        return "roles/index";
    }

    @RequestMapping(value={"/roles/save","/roles/save/{id}"}, method = RequestMethod.GET)
    public String add(Model model, @PathVariable(required = false, name = "id") Long id) {
        if (null != id) {
            model.addAttribute("role", roleService.get(id));
        } else {
            model.addAttribute("role", new Role());
        }
        return "roles/save";
    }

    @RequestMapping(value = "/roles/save", method = RequestMethod.POST)
    public String insert(@ModelAttribute Role role, Model model) {
        if(role.getId() > 0) {
            roleService.edit(role);
        }
        else {
            roleService.create(role);
        }
        model.addAttribute("roles", roleService.getAll());
        return "roles/index";
    }

    @RequestMapping(value = "/roles/delete/{id}", method = RequestMethod.GET)
    public String delete(Model model, @PathVariable(required = true, name = "id") Long id) {
        roleService.delete(id);

        model.addAttribute("roles", roleService.getAll());
        return "roles/index";
    }
}
