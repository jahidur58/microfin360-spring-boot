package com.datasoft.microfin360.springboot.presentation.controller;


import com.datasoft.microfin360.springboot.model.DivisionModel;
import com.datasoft.microfin360.springboot.service.impl.DivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by farzana on 9/27/18.
 */
@Controller
public class DivisionController {
    @Autowired
    private DivisionService divisionService;
    @RequestMapping(value = "/division", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("divisionList",divisionService.getAllDivisions());
        return "division/index";
    }

    @RequestMapping(path = "/division/add")
    public String add(Model model){
        model.addAttribute("division",new DivisionModel());
        return "division/save";
    }

    @GetMapping(path = "/division/edit/{id}")
    public String edit(Model model, @PathVariable int id){
        DivisionModel divisionModel = divisionService.findDivisionById(id);
        model.addAttribute("division",divisionModel);
        return "division/save";
    }
    @PostMapping("/division")
    public String divisionSave(@RequestParam(name="divisionId") int divisionId,@RequestParam(name = "divisionName") String divisionName,Model model) {
        DivisionModel divisionModel = new DivisionModel();
        divisionModel.setDivisionName(divisionName);
        if(divisionId > 0){
            divisionModel.setDivisionId(divisionId);
            divisionService.editDivision(divisionModel);
        }else{
            divisionService.addDivision(divisionModel);
        }
        model.addAttribute("divisionList",divisionService.getAllDivisions());
        return "division/index";
    }

    @GetMapping(path = "/division/delete/{id}")
    public String delete(Model model,@PathVariable int id){
        divisionService.deleteDivision(id);
        model.addAttribute("divisionList",divisionService.getAllDivisions());
        return "division/index";
    }
}
