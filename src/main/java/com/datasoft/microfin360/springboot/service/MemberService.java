package com.datasoft.microfin360.springboot.service;

import com.datasoft.microfin360.springboot.model.Member;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */
public interface MemberService {
    Member create(Member role);
    Member get(Long id);
    Member edit(Member role);
    void delete(Member role);
    void delete(Long id);
    List<Member> getAll(int pageNumber, int pageSiz);
    List<Member> getAll();
}
