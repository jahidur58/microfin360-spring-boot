package com.datasoft.microfin360.springboot.service.impl;

import com.datasoft.microfin360.springboot.model.Member;
import com.datasoft.microfin360.springboot.repository.MemberRepository;
import com.datasoft.microfin360.springboot.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public Member create(Member member) {
        return memberRepository.save(member);
    }

    @Override
    public Member get(Long id) {
        return memberRepository.find(id);
    }

    @Override
    public Member edit(Member member) {
        return memberRepository.update(member);
    }

    @Override
    public void delete(Member member) {

    }

    @Override
    public void delete(Long id) {
        memberRepository.delete(id);
    }

    @Override
    public List<Member> getAll(int pageNumber, int pageSiz) {
        return null;
    }

    @Override
    public List<Member> getAll() {
        return memberRepository.all();
    }

}
