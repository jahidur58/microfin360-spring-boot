package com.datasoft.microfin360.springboot.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by rayhan on 7/23/18.
 */
public class GeneratePassword {
    public static void main(String[] args) {
            String password = "admin";
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            System.out.println("password:"+passwordEncoder.encode(password));
    }
}
