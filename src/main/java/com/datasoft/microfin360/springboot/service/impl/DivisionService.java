package com.datasoft.microfin360.springboot.service.impl;


import com.datasoft.microfin360.springboot.model.DivisionModel;
import com.datasoft.microfin360.springboot.repository.impl.DivisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by farzana on 9/27/18.
 */
@Service
public class DivisionService {
    @Autowired
    private DivisionRepository divisionRepository;

    public void addDivision(DivisionModel divisionModel) {
        divisionRepository.addDivision(divisionModel);
    }

    public List<DivisionModel> getAllDivisions(){
        return divisionRepository.getAllDivisions();
    }

    public DivisionModel findDivisionById(int id){
        return divisionRepository.findDivisionById(id);
    }

    public void editDivision(DivisionModel divisionModel){
         divisionRepository.editDivision(divisionModel);
    }

    public void deleteDivision(int id){
        divisionRepository.deleteDivision(id);
    }
}
