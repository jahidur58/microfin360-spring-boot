package com.datasoft.microfin360.springboot.service.impl;

import com.datasoft.microfin360.springboot.model.User;
import com.datasoft.microfin360.springboot.repository.RoleRepository;
import com.datasoft.microfin360.springboot.repository.UserRepository;
import com.datasoft.microfin360.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jahid on 9/24/18.
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User create(User user) {
        return userRepository.insert(user);
    }

    @Override
    public User get(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User get(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public User edit(User user) {
        return userRepository.update(user);
    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);
    }

    @Override
    public List<User> getAll(int pageNumber, int pageSiz) {
        return null;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}
