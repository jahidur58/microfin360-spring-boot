package com.datasoft.microfin360.springboot.service;

import com.datasoft.microfin360.springboot.model.Role;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */
public interface RoleService {
    Role create(Role role);
    Role get(Long id);
    Role edit(Role role);
    void delete(Role role);
    void delete(Long id);
    List<Role> getAll(int pageNumber, int pageSiz);
    List<Role> getAll();
}
