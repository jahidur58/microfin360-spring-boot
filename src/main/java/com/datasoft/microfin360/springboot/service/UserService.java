package com.datasoft.microfin360.springboot.service;

import com.datasoft.microfin360.springboot.model.User;

import java.util.List;

/**
 * Created by jahid on 9/24/18.
 */
public interface UserService {
    User create(User user);
    User get(Long id);
    User get(String userName);
    User edit(User user);
    void delete(User user);
    void delete(Long id);
    List<User> getAll(int pageNumber, int pageSiz);
    List<User> getAll();
}
