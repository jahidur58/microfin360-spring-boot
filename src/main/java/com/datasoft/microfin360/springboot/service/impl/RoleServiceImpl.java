package com.datasoft.microfin360.springboot.service.impl;

import com.datasoft.microfin360.springboot.model.Role;
import com.datasoft.microfin360.springboot.repository.RoleRepository;
import com.datasoft.microfin360.springboot.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role create(Role role) {
        return roleRepository.insert(role);
    }

    @Override
    public Role get(Long id) {
        return roleRepository.findById(id);
    }

    @Override
    public Role edit(Role role) {
        return roleRepository.update(role);
    }

    @Override
    public void delete(Role role) {

    }

    @Override
    public void delete(Long id) {
        roleRepository.delete(id);
    }

    @Override
    public List<Role> getAll(int pageNumber, int pageSiz) {
        return null;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }

}
