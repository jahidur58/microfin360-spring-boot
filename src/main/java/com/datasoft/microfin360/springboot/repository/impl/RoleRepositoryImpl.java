package com.datasoft.microfin360.springboot.repository.impl;

import com.datasoft.microfin360.springboot.model.Role;
import com.datasoft.microfin360.springboot.model.rowmapper.RoleRowMapper;
import com.datasoft.microfin360.springboot.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.*;


/**
 * Created by jahid on 9/26/18.
 */

@Slf4j
@Repository
public class RoleRepositoryImpl implements RoleRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public Role insert(Role role) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("user_roles")
                .usingGeneratedKeyColumns("id");

        Map<String, Object> parameterMap = new HashMap<String, Object>();

        parameterMap.put("role_name", role.getName());
        parameterMap.put("role_description", role.getDescription());

        log.info("Role Added With Parameter: {}", parameterMap);

        try {
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if (autoGenId != null) {
                role.setId(autoGenId.intValue());
                log.info("Role Added With ID: {}", autoGenId);
                return role;
            } else {
                return role;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            log.info("Role Data Did not Added: {}", e.getMessage());
            return role;
        }
    }

    @Override
    public Role update(Role role) {
        String query = "update user_roles set role_name = ? , role_description = ? where id = ?";
        jdbcTemplate.update(query, role.getName(), role.getDescription(), role.getId());

        return role;
    }

    @Override
    public Role findById(long id) {
        String query = "SELECT * FROM user_roles WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(query, new Object[]{id}, new RoleRowMapper());
        } catch (DataAccessException e) {
            log.error("Roles query execution failed: {}.  no: {}.  Error: {}",
                    query, id, e.getMessage());
            return new Role();
        }
    }

    @Override
    public List<Role> findAll() {
        String query = "SELECT * FROM user_roles";
        try {
            return jdbcTemplate.query(query, new RoleRowMapper());
        } catch (DataAccessException e) {
            log.error("Roles query execution failed: {}. Error: {}", query, e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void delete(long id) {
        String query = "Delete  FROM user_roles where id=?";

        try {
             jdbcTemplate.update(query, id);
        } catch (DataAccessException e) {
            log.error("Roles query execution failed: {}. Error: {}", query, e.getMessage());

        }
    }
}
