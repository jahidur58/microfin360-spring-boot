package com.datasoft.microfin360.springboot.repository;

import com.datasoft.microfin360.springboot.model.User;

import java.util.List;

/**
 * Created by jahid on 9/24/18.
 */
public interface UserRepository {
    User insert(User user);

    User update(User user);

    User findById(long id);

    User findByUserName(String userName);

    List<User> findAll();

    void delete(long id);
}
