package com.datasoft.microfin360.springboot.repository.impl;


import com.datasoft.microfin360.springboot.model.DivisionModel;
import com.datasoft.microfin360.springboot.model.rowmapper.DivisionRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farzana on 9/27/18.
 */

@Repository
public class DivisionRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void addDivision(DivisionModel divisionModel) {
        String query = "INSERT INTO po_divisions(name) VALUES(?)";
        jdbcTemplate.update(query, divisionModel.getDivisionName());

    }

    public List<DivisionModel> getAllDivisions() {
        String divisionSql = "SELECT * FROM po_divisions";
        RowMapper<DivisionModel> rowMapper = new DivisionRowMapper();
        try{
            List<DivisionModel> divisions = jdbcTemplate.query(divisionSql, rowMapper);
            return divisions;
        }
        catch(Exception e){
            return new ArrayList<>();
        }

    }

    public DivisionModel findDivisionById(int id) {
        String query = "SELECT * FROM po_divisions WHERE id = ?";
        RowMapper<DivisionModel> rowMapper = new DivisionRowMapper();
        DivisionModel division = (DivisionModel)jdbcTemplate.queryForObject(query, new Object[] { id }, rowMapper);
        return division;
    }
    public void editDivision(DivisionModel divisionModel) {
        String query = "UPDATE po_divisions SET name = ? WHERE id=?";
        jdbcTemplate.update(query, divisionModel.getDivisionName(), divisionModel.getDivisionId());

    }


    public void deleteDivision(int id) {
        String query = "DELETE FROM po_divisions WHERE id=?";
        jdbcTemplate.update(query, id);
    }
}
