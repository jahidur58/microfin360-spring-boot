package com.datasoft.microfin360.springboot.repository.impl;

import com.datasoft.microfin360.springboot.model.Role;
import com.datasoft.microfin360.springboot.model.User;
import com.datasoft.microfin360.springboot.model.rowmapper.RoleRowMapper;
import com.datasoft.microfin360.springboot.model.rowmapper.UserRowMapper;
import com.datasoft.microfin360.springboot.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jahid on 9/24/18.
 */
@Slf4j
@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public User insert(User user) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("users")
                .usingGeneratedKeyColumns("id");

        Map<String, Object> parameterMap = new HashMap<String, Object>();

        parameterMap.put("full_name", user.getName());
        parameterMap.put("login", user.getUserName());
        parameterMap.put("password", passwordEncoder.encode(user.getPassword()));
        parameterMap.put("role_id", user.getRoleId());
        parameterMap.put("default_language", "english");

        log.info("User Added With Parameter: {}", parameterMap);

        try {
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if (autoGenId != null) {
                user.setId(autoGenId.intValue());
                log.info("User Added With ID: {}", autoGenId);
                return user;
            } else {
                return user;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            log.info("User Data Did not Added: {}", e.getMessage());
            return user;
        }
    }

    @Override
    public User update(User user) {
        String query = "update users set full_name = ?  where id = ?";
        jdbcTemplate.update(query, user.getName(), user.getId());

        return user;
    }

    @Override
    public User findById(long id) {
        String query = "SELECT * FROM users WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(query, new Object[]{id}, new UserRowMapper());
        } catch (DataAccessException e) {
            log.error("User query execution failed: {}. userNmae: {}.  Error: {}",
                    query, id, e.getMessage());
            return new User();
        }
    }

    @Override
    public User findByUserName(String userName) {
        String query = "SELECT * FROM users WHERE login = ?";
        try {
            return jdbcTemplate.queryForObject(query, new Object[]{userName}, new UserRowMapper());
        } catch (DataAccessException e) {
            log.error("User query execution failed: {}. userNmae: {}.  Error: {}",
                    query, userName, e.getMessage());
            return new User();
        }
    }

    @Override
    public List<User> findAll() {
        String query = "SELECT * FROM users";
        try {
            return jdbcTemplate.query(query, new UserRowMapper());
        } catch (DataAccessException e) {
            log.error("User query execution failed: {}. Error: {}", query, e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void delete(long id) {
        String query = "Delete  FROM users where id=?";

        try {
            jdbcTemplate.update(query, id);
        } catch (DataAccessException e) {
            log.error("Users query execution failed: {}. Error: {}", query, e.getMessage());

        }
    }
}
