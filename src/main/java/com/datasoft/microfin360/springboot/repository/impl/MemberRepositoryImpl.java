package com.datasoft.microfin360.springboot.repository.impl;

import com.datasoft.microfin360.springboot.model.Member;
import com.datasoft.microfin360.springboot.model.rowmapper.MemberRowMapper;
import com.datasoft.microfin360.springboot.repository.MemberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by jahid on 9/26/18.
 */

@Slf4j
@Repository
public class MemberRepositoryImpl implements MemberRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public Member save(Member member) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("members")
                .usingGeneratedKeyColumns("id");

        Map<String, Object> parameterMap = new HashMap<String, Object>();

        parameterMap.put("name", member.getName());
        parameterMap.put("father_name", member.getFatherName());
        parameterMap.put("mother_name", member.getMotherName());
        parameterMap.put("date_of_birth", member.getDob());
        parameterMap.put("samity_id", member.getSamityId());
        parameterMap.put("branch_id", member.getBranchId());

        log.info("Member Added With Parameter: {}", parameterMap);

        try {
            Number autoGenId = jdbcInsert.executeAndReturnKey(parameterMap);
            if (autoGenId != null) {
                member.setId(autoGenId.intValue());
                log.info("Member Added With ID: {}", autoGenId);
                return member;
            } else {
                return member;
            }
        } catch (DataAccessException e) {
            log.error(e.getMessage());
            log.info("Member Data Did not Added: {}", e.getMessage());
            return member;
        }
    }

    @Override
    public Member update(Member member) {
        String query = "UPDATE members set name = ?, father_name = ?, mother_name = ?, date_of_birth = ?, samity_id = ?, branch_id = ? WHERE id = ?";

        jdbcTemplate.update(query, member.getName(), member.getFatherName(), member.getMotherName(), member.getDob(), member.getSamityId(), member.getBranchId(), member.getId());

        return member;
    }

    @Override
    public Member find(long id) {
        String query = "SELECT * FROM members WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(query, new Object[]{id}, new MemberRowMapper());
        } catch (DataAccessException e) {
            log.error("Meter query execution failed: {}. meter no: {}.  Error: {}",
                    query, id, e.getMessage());
            return new Member();
        }
    }

    @Override
    public List<Member> all() {
        String query = "SELECT * FROM members WHERE 1";

        try {
            return jdbcTemplate.query(query, new MemberRowMapper());
        } catch (DataAccessException e) {
            log.error("Query execution failed: {}. Error: {}", query, e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public void delete(long id) {
        String query = "DELETE FROM members WHERE id = ?";

        try {

             jdbcTemplate.update(query, id);
        } catch (DataAccessException e) {
            log.error("Query execution failed: {}. Error: {}", query, e.getMessage());
        }

    }
}
