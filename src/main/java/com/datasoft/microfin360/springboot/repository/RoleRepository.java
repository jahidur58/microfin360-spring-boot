package com.datasoft.microfin360.springboot.repository;

import com.datasoft.microfin360.springboot.model.Role;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */
public interface RoleRepository {

    Role insert(Role role);

    Role update(Role role);

    Role findById(long id);

    List<Role> findAll();

    void delete(long id);


}
