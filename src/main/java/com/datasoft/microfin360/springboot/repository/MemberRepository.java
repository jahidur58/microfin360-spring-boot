package com.datasoft.microfin360.springboot.repository;

import com.datasoft.microfin360.springboot.model.Member;

import java.util.List;

/**
 * Created by jahid on 9/26/18.
 */
public interface MemberRepository {

    Member save(Member member);

    Member update(Member member);

    Member find(long id);

    List<Member> all();

    void delete(long id);


}
