package com.datasoft.microfin360.springboot.model.rowmapper;

import com.datasoft.microfin360.springboot.model.Member;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by jahid on 9/26/18.
 */
public class MemberRowMapper implements RowMapper<Member> {
    @Override
    public Member mapRow(ResultSet resultSet, int i) throws SQLException {
        Member member = new Member();
        member.setId(resultSet.getLong("id"));
        member.setName(resultSet.getString("name"));
        member.setFatherName(resultSet.getString("father_name"));
        member.setMotherName(resultSet.getString("mother_name"));
        member.setDob(resultSet.getString("date_of_birth"));
        member.setSamityId(resultSet.getInt("branch_id"));
        member.setBranchId(resultSet.getInt("samity_id"));
        return member;
    }
}
