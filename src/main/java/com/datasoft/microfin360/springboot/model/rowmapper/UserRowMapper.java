package com.datasoft.microfin360.springboot.model.rowmapper;

import com.datasoft.microfin360.springboot.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by jahid on 9/26/18.
 */
public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setName(resultSet.getString("full_name"));
        user.setUserName(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setRoleId(resultSet.getInt("role_id"));
        user.setLanguage(resultSet.getString("default_language"));
        return user;
    }
}
