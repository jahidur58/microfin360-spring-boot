package com.springMicrofin360.microfin.model.rowmapper;

import com.springMicrofin360.microfin.model.LoanProduct;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoanProductRowMapper implements RowMapper<LoanProduct> {
    public LoanProduct mapRow(ResultSet rs,int rowNum) throws SQLException {
        LoanProduct loanProduct=new LoanProduct();
        loanProduct.setId(rs.getLong("id"));
        loanProduct.setShort_name(rs.getString("short_name"));
        loanProduct.setCode(rs.getString("code"));
        loanProduct.setLoan_product_category_id(rs.getLong("loan_product_category_id"));
        loanProduct.setIs_primary_product(rs.getBoolean("is_primary_product"));
        loanProduct.setStart_date(rs.getDate("start_date"));
        loanProduct.setMinimum_loan_amount(rs.getInt("minimum_loan_amount"));
        loanProduct.setMaximum_loan_amount(rs.getInt("maximum_loan_amount"));
        loanProduct.setDefault_loan_amount(rs.getInt("default_loan_amount"));
        loanProduct.setFunding_organization_id(rs.getInt("funding_organization_id"));
        loanProduct.setGrace_period(rs.getString("grace_period"));
        loanProduct.setIs_insurance_applicable(rs.getBoolean("is_insurance_applicable"));
        loanProduct.setInsurance_calculation_method(rs.getInt("insurance_calculation_method"));
        loanProduct.setAdditional_fee(rs.getBigDecimal("additional_fee"));
        loanProduct.setMaximum_insurance_amount(rs.getBigDecimal("maximum_insurance_amount"));
        loanProduct.setInsurance_amount(rs.getBigDecimal("insurance_amount"));
        loanProduct.setInsurance_amount_1(rs.getBigDecimal("insurance_amount_1"));
        loanProduct.setNumber_of_installment(rs.getString("number_of_installment"));
        loanProduct.setLoan_product_type(rs.getString("loan_product_type"));
        loanProduct.setRepayment_frequency(rs.getString("repayment_frequency"));
        loanProduct.setEligible_repayment_frequency(rs.getString("eligible_repayment_frequency"));
        loanProduct.setInterest_payment_frequency(rs.getString("interest_payment_frequency"));
        loanProduct.setMode_of_monthly_collection(rs.getString("mode_of_monthly_collection"));
        loanProduct.setMonthly_collection_day_or_week(rs.getString("monthly_collection_day_or_week"));
        loanProduct.setWrite_off_eligible_years(rs.getInt("write_off_eligible_years"));
        loanProduct.setLoan_form_fee(rs.getBigDecimal("loan_form_fee"));
        loanProduct.setHealth_expenditure(rs.getBigDecimal("health_expenditure"));
        loanProduct.setRisk_insurance(rs.getBigDecimal("risk_insurance"));
        loanProduct.setAdditional_fee(rs.getBigDecimal("additional_fee"));
        loanProduct.setMaximum_loan_amount_for_additional_fee_applicable(rs.getBigDecimal("maximum_loan_amount_for_additional_fee_applicable"));
        loanProduct.setMaximum_loan_amount_for_insurance_applicable(rs.getBigDecimal("maximum_loan_amount_for_insurance_applicable"));
        loanProduct.setSkip_last_week_calculation_monthly(rs.getBoolean("skip_last_week_calculation_monthly"));
        loanProduct.setAdditional_fee_calculation_method(rs.getInt("additional_fee_calculation_method"));
        loanProduct.setMandatory_savings_amount(rs.getBigDecimal("mandatory_savings_amount"));
        return loanProduct;
    }
}
