package com.datasoft.microfin360.springboot.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by farzana on 9/27/18.
 */

public class DivisionModel {

    private int divisionId;
    @NotNull @NotEmpty(message = "Division name required")
    private String divisionName;

    public Integer getDivisionId() {
        return divisionId;
    }
    public void setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
    }
    public String getDivisionName() {
        return divisionName;
    }
    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

}
