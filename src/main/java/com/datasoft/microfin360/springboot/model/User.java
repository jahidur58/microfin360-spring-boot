package com.datasoft.microfin360.springboot.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by jahid on 9/24/18.
 */

@Data
public class User {
    private long id;
    private String name;
    private String userName;
    private String password;
    private String confirmPassword;
    private int roleId;
    private String language;
}
