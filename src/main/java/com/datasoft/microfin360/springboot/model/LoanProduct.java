package com.springMicrofin360.microfin.model;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
/**
 * Created by jahid on 9/24/18.
 */
@Data
public class LoanProduct {
    private Long id;
    private String short_name;
    private String code;
    private Long loan_product_category_id;
    private boolean is_primary_product;
    private Date start_date;
    private int minimum_loan_amount;
    private int maximum_loan_amount;
    private int default_loan_amount;
    private int funding_organization_id;
    private String grace_period;
    private boolean is_insurance_applicable;
    private int insurance_calculation_method;
    private BigDecimal maximum_insurance_amount;
    private BigDecimal insurance_amount;
    private BigDecimal insurance_amount_1;
    private String number_of_installment;
    private String loan_product_type;
    private String repayment_frequency;
    private String eligible_repayment_frequency;
    private String interest_payment_frequency;
    private String mode_of_monthly_collection;
    private String monthly_collection_day_or_week;
    private int write_off_eligible_years;
    private BigDecimal loan_form_fee;
    private BigDecimal health_expenditure;
    private BigDecimal risk_insurance;
    private BigDecimal additional_fee;
    private BigDecimal maximum_loan_amount_for_additional_fee_applicable;
    private BigDecimal maximum_loan_amount_for_insurance_applicable;
    private boolean skip_last_week_calculation_monthly;
    private int additional_fee_calculation_method;
    private BigDecimal mandatory_savings_amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getLoan_product_category_id() {
        return loan_product_category_id;
    }

    public void setLoan_product_category_id(Long loan_product_category_id) {
        this.loan_product_category_id = loan_product_category_id;
    }

    public boolean isIs_primary_product() {
        return is_primary_product;
    }

    public void setIs_primary_product(boolean is_primary_product) {
        this.is_primary_product = is_primary_product;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public int getMinimum_loan_amount() {
        return minimum_loan_amount;
    }

    public void setMinimum_loan_amount(int minimum_loan_amount) {
        this.minimum_loan_amount = minimum_loan_amount;
    }

    public int getMaximum_loan_amount() {
        return maximum_loan_amount;
    }

    public void setMaximum_loan_amount(int maximum_loan_amount) {
        this.maximum_loan_amount = maximum_loan_amount;
    }

    public int getDefault_loan_amount() {
        return default_loan_amount;
    }

    public void setDefault_loan_amount(int default_loan_amount) {
        this.default_loan_amount = default_loan_amount;
    }

    public int getFunding_organization_id() {
        return funding_organization_id;
    }

    public void setFunding_organization_id(int funding_organization_id) {
        this.funding_organization_id = funding_organization_id;
    }

    public String getGrace_period() {
        return grace_period;
    }

    public void setGrace_period(String grace_period) {
        this.grace_period = grace_period;
    }

    public boolean isIs_insurance_applicable() {
        return is_insurance_applicable;
    }

    public void setIs_insurance_applicable(boolean is_insurance_applicable) {
        this.is_insurance_applicable = is_insurance_applicable;
    }

    public int getInsurance_calculation_method() {
        return insurance_calculation_method;
    }

    public void setInsurance_calculation_method(int insurance_calculation_method) {
        this.insurance_calculation_method = insurance_calculation_method;
    }

    public BigDecimal getMaximum_insurance_amount() {
        return maximum_insurance_amount;
    }

    public void setMaximum_insurance_amount(BigDecimal maximum_insurance_amount) {
        this.maximum_insurance_amount = maximum_insurance_amount;
    }

    public BigDecimal getInsurance_amount() {
        return insurance_amount;
    }

    public void setInsurance_amount(BigDecimal insurance_amount) {
        this.insurance_amount = insurance_amount;
    }

    public BigDecimal getInsurance_amount_1() {
        return insurance_amount_1;
    }

    public void setInsurance_amount_1(BigDecimal insurance_amount_1) {
        this.insurance_amount_1 = insurance_amount_1;
    }

    public String getNumber_of_installment() {
        return number_of_installment;
    }

    public void setNumber_of_installment(String number_of_installment) {
        this.number_of_installment = number_of_installment;
    }

    public String getLoan_product_type() {
        return loan_product_type;
    }

    public void setLoan_product_type(String loan_product_type) {
        this.loan_product_type = loan_product_type;
    }

    public String getRepayment_frequency() {
        return repayment_frequency;
    }

    public void setRepayment_frequency(String repayment_frequency) {
        this.repayment_frequency = repayment_frequency;
    }

    public String getEligible_repayment_frequency() {
        return eligible_repayment_frequency;
    }

    public void setEligible_repayment_frequency(String eligible_repayment_frequency) {
        this.eligible_repayment_frequency = eligible_repayment_frequency;
    }

    public String getInterest_payment_frequency() {
        return interest_payment_frequency;
    }

    public void setInterest_payment_frequency(String interest_payment_frequency) {
        this.interest_payment_frequency = interest_payment_frequency;
    }

    public String getMode_of_monthly_collection() {
        return mode_of_monthly_collection;
    }

    public void setMode_of_monthly_collection(String mode_of_monthly_collection) {
        this.mode_of_monthly_collection = mode_of_monthly_collection;
    }

    public String getMonthly_collection_day_or_week() {
        return monthly_collection_day_or_week;
    }

    public void setMonthly_collection_day_or_week(String monthly_collection_day_or_week) {
        this.monthly_collection_day_or_week = monthly_collection_day_or_week;
    }

    public int getWrite_off_eligible_years() {
        return write_off_eligible_years;
    }

    public void setWrite_off_eligible_years(int write_off_eligible_years) {
        this.write_off_eligible_years = write_off_eligible_years;
    }

    public BigDecimal getLoan_form_fee() {
        return loan_form_fee;
    }

    public void setLoan_form_fee(BigDecimal loan_form_fee) {
        this.loan_form_fee = loan_form_fee;
    }

    public BigDecimal getHealth_expenditure() {
        return health_expenditure;
    }

    public void setHealth_expenditure(BigDecimal health_expenditure) {
        this.health_expenditure = health_expenditure;
    }

    public BigDecimal getRisk_insurance() {
        return risk_insurance;
    }

    public void setRisk_insurance(BigDecimal risk_insurance) {
        this.risk_insurance = risk_insurance;
    }

    public BigDecimal getAdditional_fee() {
        return additional_fee;
    }

    public void setAdditional_fee(BigDecimal additional_fee) {
        this.additional_fee = additional_fee;
    }

    public BigDecimal getMaximum_loan_amount_for_additional_fee_applicable() {
        return maximum_loan_amount_for_additional_fee_applicable;
    }

    public void setMaximum_loan_amount_for_additional_fee_applicable(BigDecimal maximum_loan_amount_for_additional_fee_applicable) {
        this.maximum_loan_amount_for_additional_fee_applicable = maximum_loan_amount_for_additional_fee_applicable;
    }

    public BigDecimal getMaximum_loan_amount_for_insurance_applicable() {
        return maximum_loan_amount_for_insurance_applicable;
    }

    public void setMaximum_loan_amount_for_insurance_applicable(BigDecimal maximum_loan_amount_for_insurance_applicable) {
        this.maximum_loan_amount_for_insurance_applicable = maximum_loan_amount_for_insurance_applicable;
    }

    public boolean getSkip_last_week_calculation_monthly() {
        return skip_last_week_calculation_monthly;
    }

    public void setSkip_last_week_calculation_monthly(boolean skip_last_week_calculation_monthly) {
        this.skip_last_week_calculation_monthly = skip_last_week_calculation_monthly;
    }

    public int getAdditional_fee_calculation_method() {
        return additional_fee_calculation_method;
    }

    public void setAdditional_fee_calculation_method(int additional_fee_calculation_method) {
        this.additional_fee_calculation_method = additional_fee_calculation_method;
    }

    public BigDecimal getMandatory_savings_amount() {
        return mandatory_savings_amount;
    }

    public void setMandatory_savings_amount(BigDecimal mandatory_savings_amount) {
        this.mandatory_savings_amount = mandatory_savings_amount;
    }
}
