package com.datasoft.microfin360.springboot.model.rowmapper;




import com.datasoft.microfin360.springboot.model.DivisionModel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DivisionRowMapper implements RowMapper<DivisionModel> {
    @Override
    public DivisionModel mapRow(ResultSet rs, int rowNum) throws SQLException {
        DivisionModel divisionModel = new DivisionModel();
        divisionModel.setDivisionId(rs.getInt("id"));
        divisionModel.setDivisionName(rs.getString("name"));
        return divisionModel;
    }
}
