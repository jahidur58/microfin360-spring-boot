package com.datasoft.microfin360.springboot.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by jahid on 9/24/18.
 */
@Data
public class Member {
    private long id;
    private String name;
    private String fatherName;
    private String motherName;
    private String dob;
    private int branchId;
    private int samityId;
}
