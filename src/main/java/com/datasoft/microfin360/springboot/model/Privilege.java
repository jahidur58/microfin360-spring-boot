package com.datasoft.microfin360.springboot.model;

import lombok.Data;

/**
 * Created by jahid on 9/24/18.
 */

@Data
public class Privilege {
    private long id;
    private String name;
    private Role role;

}
